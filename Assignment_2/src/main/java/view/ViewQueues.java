package view;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class ViewQueues {

	private JFrame frame;
	private JTextField minAT;
	private JTextField maxAT;
	private JLabel lblMin;
	private JLabel lblMax;
	private JLabel lblPleaseIntroduceHere;
	private JTextField minST;
	private JTextField maxST;
	private JLabel lblMinService;
	private JLabel lblMaxService;
	private JLabel lblForThisSimulation;
	private JLabel lblSimulationTimeWill;
	JComboBox<Integer> queues;
	JComboBox<Integer> simulation;
	public JButton btnStartSimulation; //i need to see it in controller asta ii prima

	public ViewQueues() { //constructor with 1 method call
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		minAT = new JTextField();
		minAT.setBounds(138, 43, 116, 22);
		frame.getContentPane().add(minAT);
		minAT.setColumns(10);
		
		maxAT = new JTextField();
		maxAT.setBounds(369, 43, 116, 22);
		frame.getContentPane().add(maxAT);
		maxAT.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Please introduce here the minimum and maximum arrival time of a client");
		lblNewLabel.setBounds(68, 14, 446, 16);
		frame.getContentPane().add(lblNewLabel);
		
		lblMin = new JLabel("min arrival");
		lblMin.setBounds(68, 46, 76, 16);
		frame.getContentPane().add(lblMin);
		 
		lblMax = new JLabel("max arrival");
		lblMax.setBounds(284, 46, 94, 16);
		frame.getContentPane().add(lblMax);
		
		lblPleaseIntroduceHere = new JLabel("Please introduce here the minimum anx maximum service time for a client");
		lblPleaseIntroduceHere.setBounds(68, 101, 446, 16);
		frame.getContentPane().add(lblPleaseIntroduceHere);
		
		minST = new JTextField();
		minST.setBounds(138, 140, 116, 22);
		frame.getContentPane().add(minST);
		minST.setColumns(10);
		
		maxST = new JTextField();
		maxST.setBounds(369, 140, 116, 22);
		frame.getContentPane().add(maxST);
		maxST.setColumns(10);
		
		lblMinService = new JLabel("min service");
		lblMinService.setBounds(68, 143, 76, 16);
		frame.getContentPane().add(lblMinService);
		
		lblMaxService = new JLabel("max service");
		lblMaxService.setBounds(284, 143, 81, 16);
		frame.getContentPane().add(lblMaxService);
		
		lblForThisSimulation = new JLabel("For this simulation the number of queues is: ");
		lblForThisSimulation.setBounds(68, 195, 286, 16);
		frame.getContentPane().add(lblForThisSimulation);
		
		Integer[] possibilities= {1, 2, 3, 4, 5, 6};
		queues = new JComboBox(possibilities);
		queues.setBounds(380, 192, 105, 22);
		frame.getContentPane().add(queues);
		queues.setSelectedIndex(5);
		
		lblSimulationTimeWill = new JLabel("Simulation time will be:");
		lblSimulationTimeWill.setBounds(74, 254, 162, 16);
		frame.getContentPane().add(lblSimulationTimeWill);
		
		Integer[] times= {10, 15, 20, 25, 30, 35, 40, 45, 50};
		simulation = new JComboBox(times);
		simulation.setBounds(380, 251, 54, 22);
		frame.getContentPane().add(simulation);
		
		btnStartSimulation = new JButton("Start Simulation");
		btnStartSimulation.setBounds(207, 300, 147, 25);
		frame.getContentPane().add(btnStartSimulation);
		frame.setVisible(true);		
	}
	
	public int getMinAT() {
		try {
		return Integer.parseInt(minAT.getText());			
		} catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(frame, "Wrong minimum arrival time. Please inproduce a valid value!", "Warning", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
		
		return 0;	
		}
	public int getMaxAT() {
		try {
		return Integer.parseInt(maxAT.getText());			
		}catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(frame, "Wrong maximum arrival time. Please inproduce a valid value!", "Warning", JOptionPane.WARNING_MESSAGE);	
			System.exit(0);
		}
		return 0;	
		}
	public int getMinST() {
		try {
		return Integer.parseInt(minST.getText());			
		}catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(frame, "Wrong minimum service time. Please inproduce a valid value!", "Warning", JOptionPane.WARNING_MESSAGE);	
			System.exit(0);
		}
		return 0;	
		}
	public int getMaxST(){
		try {		
		return Integer.parseInt(maxST.getText());			
		}catch(NumberFormatException e)
		{
			JOptionPane.showMessageDialog(frame, "Wrong maximum service time. Please inproduce a valid value!", "Warning", JOptionPane.WARNING_MESSAGE);
			 //return Integer.parseInt(maxST.getText());
			System.exit(0);
		}
		//getMaxST();
		return 0;	
		}
	public int getQueuesNumber() {
		return (int) queues.getSelectedItem();		
		}
	public int getSimulationTime() {
		return (int) simulation.getSelectedItem();		
		}
}

