package view;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JPanel;

public class OtherView {
	private JFrame frame;
	private JTextField waitingTime;
	private JTextField servingTime;
	private JTextField emptyTime;
	private JTextField peakHour;
	private int nrQueues; //read from the GUI a doua 
	private ArrayList<JTextField> showQueues;
	private JTextArea logText;
	
	public OtherView() {
		initialize();
	}
	void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLog = new JLabel("  LOG");
		lblLog.setBounds(433, 13, 66, 16);
		frame.getContentPane().add(lblLog);
		
		JLabel lblAverageEmptyTime = new JLabel("Average Waiting Time:");
		lblAverageEmptyTime.setBounds(12, 29, 140, 27);
		frame.getContentPane().add(lblAverageEmptyTime);
		
		setWaitingTime(new JTextField());
		frame.getContentPane().add(getWaitingTime());
		getWaitingTime().setColumns(10);
		
		JLabel lblAverageServingTime = new JLabel("Average Serving Time:");
		lblAverageServingTime.setBounds(12, 91, 140, 16);
		frame.getContentPane().add(lblAverageServingTime);
		
		setServingTime(new JTextField());
		frame.getContentPane().add(getServingTime());
		getServingTime().setColumns(10);
		
		JLabel lblAverageEmptyTime_1 = new JLabel("Average Empty Time:");
		lblAverageEmptyTime_1.setBounds(12, 131, 140, 16);
		frame.getContentPane().add(lblAverageEmptyTime_1);
		
		setEmptyTime(new JTextField());
		frame.getContentPane().add(getEmptyTime());
		getEmptyTime().setColumns(10);
		
		JLabel lblPeakHourFor = new JLabel("Peak Hour For The Simulation Interval:");
		lblPeakHourFor.setBounds(12, 178, 256, 16);
		frame.getContentPane().add(lblPeakHourFor);
		
		peakHour = new JTextField();
		peakHour.setBounds(262, 175, 55, 22);
		frame.getContentPane().add(peakHour);
		peakHour.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBounds(338, 35, 232, 405);
		frame.getContentPane().add(panel);
		panel.setLayout(new BorderLayout());
		
		logText = new JTextArea();
				
		JScrollPane scrollPane = new JScrollPane(logText,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(211, 13, -198, 379);
		panel.add(scrollPane);
		
		JLabel lblQueues = new JLabel("Queues:");
		lblQueues.setBounds(12, 207, 56, 16);
		frame.getContentPane().add(lblQueues);
		
		//here I have an array of textField each showing me the content of a queue, but as a Srting
		showQueues = new ArrayList<JTextField>();
		for(int i=0 ; i<6 ; i++) {
			showQueues.add(new JTextField());
			(showQueues.get(i)).setBounds(12, 230+30*i, 116, 22);
			frame.getContentPane().add(showQueues.get(i));
			showQueues.get(i).setColumns(10);
		}
	}
	public void setWaitingTine(String x) {
		getWaitingTime().setText(x);
	}
	public void setServingTime(int x) {
		servingTime.setText(String.valueOf(x));
	}
	public void setEmptyTime(int x) {
		emptyTime.setText(String.valueOf(x));
	}
	public void setPeakHour(int x) {
		peakHour.setText(String.valueOf(x));
	}
	public int getNrQueues() {
		return nrQueues;
	}
	public JTextField getWaitingTime() {
		return waitingTime;
	}
	public void setWaitingTime(JTextField waitingTime) {
		this.waitingTime = waitingTime;
		waitingTime.setBounds(147, 31, 194, 22);
	}
	public JTextField getServingTime() {
		return servingTime;
	}
	public void setServingTime(JTextField servingTime) {
		this.servingTime = servingTime;
		servingTime.setBounds(147, 88, 194, 22);
	}
	public JTextField getEmptyTime() {
		return emptyTime;
	}
	public void setEmptyTime(JTextField emptyTime) {
		this.emptyTime = emptyTime;
		emptyTime.setBounds(147, 128, 155, 22);
	}
	public JFrame getFrame() {
		return frame;
	}
	public void setLogText(String logText) {
		this.logText.setText(logText);
	}
	}
