package model;

public class Client {
	private	String id;
	private int arrivalTime;
	private int serviceTime;
	
	public Client() {
		id="";
		arrivalTime=-1;
		serviceTime=-1;
	}
	
	public Client(String id,int arrivalTime,  int serviceTime) {
		this.id = id;
		this.serviceTime = serviceTime;
		this.arrivalTime= arrivalTime;///aici trebuie timpul current de cand ii adasugat in coada
	}
	public int getServiceTime() {
		return serviceTime;
	}
	public int getArrivalTime() {
		return arrivalTime;
	}
	public String toString() { //printing
		return "Client "+id+" ar="+arrivalTime+" st="+serviceTime;
	}
}

