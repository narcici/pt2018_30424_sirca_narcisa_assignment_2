package model;

import java.util.ArrayList;
import java.util.Random;

public class Store extends Thread {
	public static ArrayList<Queue> cozi;
	Queue waitingClients =new Queue(); // here I put all the waiting clients I generate
	final private  int  minAT; // minimum arriving time
	final private  int  maxAT; // maxim arriving time
	final private int minST; // minimum processing time 
	final private int maxST; // maxim processing time     all 4 variables ar cond. for clients
	final private int nrQueues;
	public static int simulationTime;  // static bc I need to check them in Queue too
	public static int currentTime;
	public static int[] serviceTimesQueues=new int[10]; // array of waiting time for every queue
	public static int[] waitingTimeQueues=new int[10]; //sums of waiting times for each queue // when start processing- arrival time
	public static int[] sizesOfQueues=new int[10]; //initial sizes of queues
	static int min=0;
	double[] mediileServing=new double[10];
	double[] mediileWaiting=new double[10];
	private StringBuffer serviceA=new StringBuffer();
	private StringBuffer waitingMedii=new StringBuffer();
	private	StringBuffer emptyA=new StringBuffer();
	private static StringBuffer log= new StringBuffer("The simulation is at it follows:\n");
	
	// I'll have to set them after I read them as inputs from GUI
	public Store(int simulationTime, int nrQueues, int minAT, int maxAT, int minST, int maxST){
		cozi =new ArrayList<Queue>();
		this.simulationTime= simulationTime;
		this.nrQueues = nrQueues;
		this.minAT= minAT;
		this.maxAT= maxAT;
		this.minST= minST;
		this.maxST= maxST;
		
	}
	public static ArrayList<Queue> getQueues() {  // getter for THE ARRAY OF QUEUES
		return cozi;
	}
	
	public int getNrQueues() {
		return nrQueues;
	}
	private void addQueue(Queue queue) {
		cozi.add(queue); // add a queue in the array of queues	
	}
	private Client generateClient(String nume){
		Random r=new Random();
		int arrive= r.nextInt(maxAT-minAT+1) +minAT;  // I know the AT and PT if this simulation bc they are inputs from GUI
		int process= r.nextInt(maxST-minST+1) +minST;
		Client nou= new Client(nume, arrive, process);
		return nou;
	}
	// I need to return the index of the minimal Queue from the array of Queues, so I can write it in LOG
	public int minQueue() {
		int minimal=0; 
		for(Queue it: cozi) {
			if (it.getServiceTime() < cozi.get(minimal).getServiceTime())
				minimal =cozi.indexOf(it); 
		}
		return minimal;
	}
	public void initalizeStore(int queuesNumber) {// call generateClient method here
		Random r= new Random();
		int nrClients = r.nextInt(25) +6; // minimum 6 clients, maximum 30
		for(int i=0; i< nrClients; i++) {
			waitingClients.addClient(generateClient(""+i)); // here I create a wait list with clients that need to be processed
		}
		waitingClients.sort(); //I implemented this method in QUEUE, they are sorted by the arrivalTime
		for(int i=0; i< nrQueues; i++) {
			addQueue(new Queue()); //add as much queues as the nr of nrQueues
		}
		for( int i=0; i< this.nrQueues; i++){  //initialize my variable for this simulation 
			serviceTimesQueues[i]=0;
			sizesOfQueues[i]=0;
		}
		}
	public void run() {
		initalizeStore(nrQueues); // I know the nr of queues now and start them all
		//System.out.println("INITIALIZED");
		for(Queue coada:cozi) {
			coada.start();
		}
		for(currentTime=0 ; currentTime<= simulationTime; currentTime++) {
			log.append("time: " + currentTime+"\n");
			if(! waitingClients.isEmpty()) {
				//System.out.println(waitingClients); //progres!
				Client processed = waitingClients.getFirst(); //iau din fata din coada			
				while (processed.getArrivalTime() == currentTime && ! waitingClients.isEmpty()) {
					 min = minQueue();
					cozi.get(min).addClient(processed); // il adaug in coada cu timp de asteptare minim
					serviceTimesQueues[min] = serviceTimesQueues[min] + processed.getServiceTime();//increase the total waiting time in this queue
					sizesOfQueues[min] = sizesOfQueues[min]+1; //increase the nr of elem that were processed by this queue 
					//aici mai afisez //ca in log// cand ii adaugat clientul ala la ce coada se adauga si la ce timp a fost adaugat
					log.append(processed.toString()+" was added to queue nr "+ min+" at the time "+currentTime+"\n"); 
						waitingClients.removeFirst(); // il elimin din coada celor care asteapta 	//System.out.println(cozi.get(min).toString()); // asta pare toata coada
					
					if (! waitingClients.isEmpty()) {
						processed= waitingClients.getFirst();
					}
				} 
			}
			//System.out.println(currentTime);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) { 
				e.printStackTrace();
			}
	}
	}
	public void calculateAverages() {
		//calculate and print the average serving time 
		double mediaServing =0;
		for (int i=0; i< getNrQueues() ; i++) {
			if (sizesOfQueues[i] != 0) {
				mediileServing[i] = serviceTimesQueues[i] / sizesOfQueues[i]; //total waiting time/nr of clients
			}
			mediaServing += mediileServing[i];
			serviceA.append("Q"+i+" "+mediileServing[i]+" // ");//average serving time for each queue
		}
		mediaServing = mediaServing/ getNrQueues();
		serviceA.append("for the store:"+mediaServing); //total average for serving the clients in all the queues
		//calculate and print the waiting time for each queue
		double mediaWaiting=0;		
		for (int i=0; i< getNrQueues() ; i++) {
			if (sizesOfQueues[i] != 0) {
				mediileWaiting[i] = waitingTimeQueues[i] / sizesOfQueues[i]; //total waiting time/nr of clients
			} else { 
				mediileWaiting[i] = 0;
			}
			mediaWaiting += mediileWaiting[i];
			waitingMedii.append("Q"+i+" "+mediileServing[i]+" // ");//average waiting time for each queue
		}
		mediaWaiting = mediaWaiting/ getNrQueues();
		waitingMedii.append("for the store:"+mediaServing); //total average of waiting for the clients in all the queues
		//average empty time
		double emptyAverage=0;
		for (int i=0; i< getNrQueues() ; i++) {
			emptyAverage += getQueues().get(i).getEmptyTime(); //sum of empty times
			emptyA.append("Q"+i+" "+getQueues().get(i).getEmptyTime()+" // ");//average waiting time for each queue
		}
		emptyA.append(emptyAverage/getNrQueues());
		
	}
	public String getWaitingA() {
		return waitingMedii.toString();
	}
	public String getEmptyA() {
		return emptyA.toString();
	}
	public String getServiceA() {
		return serviceA.toString();
	}
	public String getLog() {
		return log.toString();
	}
	public static void appendLog(String s) {
		log.append(s);
	}
	
	
	
	/*
	public static void main(String[] args) {
		//this.nrQueues = nrQueues;
		int minAT= 1;
		int maxAT= 7;
		int minST= 2;
		int maxST= 4;
		int nrQueues=3;
		int simulationTime=20;
		Store s= new Store(simulationTime, nrQueues,minAT, maxAT, minST, maxST);
		s.start();
	}	*/
	
}

