package model;

import java.util.ArrayDeque;//class implementing Deque which I need
import java.util.ArrayList;
import java.util.Comparator;

public class Queue extends Thread { // I choose to extend Threads
	private ArrayDeque<Client> queue ;
	private int currentServiceTime;
	private int emptyTime=0; //empty time for this queue
	// i need this comparator to sort my (ArrayList)/ ArrayDeque so that the clients will come in order by arrival time
	Comparator<Client> comparator = new ComparaClients();
	
	public Queue() {
		queue = new ArrayDeque<Client>();
		currentServiceTime=0;
	}
	
	public void addClient(Client element) {
		queue.addLast(element); // I always add elements at the end of this queue  
		calcServiceTime(); // update
	}
	public void removeClient() {
		queue.removeFirst(); // I always delete the first CLIENT from the list, which should be the first added prev.
		calcServiceTime(); // update
	}
	private void calcServiceTime() { // I calc the sum of processing time in  order to determine the min queue
		int service=0;
		for(Client it:queue) {
			service += it.getServiceTime();
		}		
		currentServiceTime = service;
	}
	public int getServiceTime() { //getterS
		return currentServiceTime;
	}
	public int size() {
		return queue.size(); //its nr of elements here
	}
	public int getEmptyTime() {
		return emptyTime;
	}
	@Override
	public String toString() {
		StringBuffer sb=new StringBuffer("");
		for(Client iss:queue) {
			sb.append(iss.toString()+"\\");
		}
		System.out.println(sb.toString());
		return sb.toString();
		
	}
	@Override
	public void run() { 
		
		emptyTime=0;
		while(Store.currentTime < Store.simulationTime) {
			if(! queue.isEmpty()) {
				
				Client client= queue.getFirst();
				Store.waitingTimeQueues[Store.min] += (Store.currentTime -client.getArrivalTime()); //increase the total waiting time on this queue
				Store.appendLog("Client in service: " + client+"\n");
				int decrementTime= client.getServiceTime(); // check how much time this client has to be processed
				while(decrementTime > 0) {
					decrementTime--;
					currentServiceTime--; //the total time decrements too
					try { // waiting for a second here
						Thread.sleep(1000);
					} catch (InterruptedException e) { 
						e.printStackTrace();
					} 
				}
				//now that the time to process this client has passed I cane remove it from queue
				Store.appendLog(queue.getFirst() + " has left at time "+Store.currentTime+"\n");
				//System.out.println("empty time "+emptyTime);
				queue.removeFirst(); 
			} else {
				emptyTime++;
					try {
					Thread.sleep(1000);
				} catch (InterruptedException e) { 
					e.printStackTrace();
				} 
			}
		
		}
	}
	void sort() { // sort an put them back in the deque
		ArrayList<Client> arraySort=new ArrayList<Client>();
		while(! queue.isEmpty()){
			
			for(Client it: queue) {
				arraySort.add(it);
				queue.removeFirst();
				}
			}
		arraySort.sort(comparator);
		while( ! arraySort.isEmpty()) {
		Client c=arraySort.get(0);
		queue.addLast(c);
		arraySort.remove(0);

		}
	}
	 // those are for the class too
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	public Client getFirst() {
		return queue.getFirst();
	} 
	public void removeFirst() {
		queue.removeFirst();
	}
	
}
class ComparaClients implements Comparator<Client> {

	public int compare(Client arg0, Client arg1) {
		if(arg0.getArrivalTime() > arg1.getArrivalTime()) 
			return 1;
		if(arg0.getArrivalTime() < arg1.getArrivalTime())
			return -1;
		return 0;
	}
}
