package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JTextField;
import model.Queue;
import model.Store;
import view.OtherView;
import view.ViewQueues;

public class Controller extends Thread { // also a thread this one
	Store store;
	ViewQueues viewBefore;
	OtherView viewAfter;
	private ArrayList<JTextField> showQueues = new ArrayList<JTextField>(); // list of textfield where I print the queues
	public Controller() {
		viewBefore= new ViewQueues();//everything that belongs to view is initialized
		viewBefore.btnStartSimulation.addActionListener(new ActionListener() { //start simulation here
			public void actionPerformed(ActionEvent e) {
				int minAT= (int)viewBefore.getMinAT(); //call this method from viewBefore thing
				int maxAT = (int)viewBefore.getMaxAT();
				int minST =(int) viewBefore.getMinST();
				int maxST =(int) viewBefore.getMaxST();
				int nrQueues =(int) viewBefore.getQueuesNumber();
				int simTime =(int) viewBefore.getSimulationTime();
				store = new Store(simTime, nrQueues, minAT, maxAT, minST, maxST); //START MY STORE, wHICH HAS NOW ITS PARAM
				System.out.println("store");
				
				for (int i = 0; i < nrQueues; i++) {
					showQueues.add(new JTextField("123"));
				}
				
				System.out.println("is in controller");
				viewAfter =new 	OtherView(); //here OtherView starts existing 
				store.start(); //here is the start, so everything starts doing Things
				viewAfter.getFrame().setVisible(true);
				start();
				}
			}); 
}					
void updateQueues(ArrayList<Queue> c) {
		for( int i=0; i< viewAfter.getNrQueues(); i++) // for every queue
		{
			//System.out.println(c.get(i).toString());
			showQueues.get(i).setText(c.get(i).toString()); //suppose it does it real time, but sure not here should do it 
		}
	}
public void run() {
		while(Store.currentTime < Store.simulationTime) {
				viewAfter.setLogText(store.getLog());
				try {
					Thread.sleep(500); // waits until it is sure the simulation of store is done
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updateQueues(store.getQueues()); //I print the queues in diff testfields
				//System.out.println("Sa"); //was a test at some
		}
	
			store.calculateAverages();
			viewAfter.getServingTime().setText(store.getServiceA());
			viewAfter.getWaitingTime().setText(store.getWaitingA());
			viewAfter.getEmptyTime().setText(store.getEmptyA());
				
//			}
//		}); 
	
	}
}
